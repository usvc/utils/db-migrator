# Database Migrator

This is the migrator used for `usvc` projects. It's based on the [`standalone_migrations`](https://github.com/thuss/standalone-migrations) which allows us to use the Rails Database Migrations in non-Rails projects.

The image can be found at [`usvc/db-migrator`](https://hub.docker.com/r/usvc/db-migrator).

# Usage

## Via Docker Image

Run the following to run commands in the Docker image:

```sh
docker run -it --network host -e RAILS_ENV=${RAILS_ENV} -v $(pwd):/app -u $(id -u) usvc/db-migrator:latest ${COMMAND}
```

- Replace `${RAILS_ENV}` with production/development
- Replace `${COMMAND}` with your command.
- `${COMMAND}` to setup the directory is `rake db:setup`
- `${COMMAND}` to migrate is `rake db:migrate`
- `${COMMAND}` to rollback is `rake db:rollback`
- `${COMMAND}` to seed is `rake db:seed`

## Via Makefile

Copy and paste the following into your `Makefile`:

```makefile
MIGRATION_IMAGE=usvc/db-migrator

db_setup:
	@$(MAKE) rake CMD="db:setup"

db_migrate:
	@$(MAKE) rake CMD="db:migrate"

db_seed:
	@$(MAKE) rake CMD="db:seed"

db_rollback:
	@$(MAKE) rake CMD="db:rollback"

rake:
	@docker run -it \
		--network host \
		--env RAILS_ENV=development \
		--volume $$(pwd):/app \
		--user $$(id -u) \
		--entrypoint rake \
		$(MIGRATION_IMAGE):latest ${CMD}
```



# Development Runbook

## Building the image

Run `make build`

Verify that the image is built by running `docker image ls | head -n 10 | grep db-migrator`.

## Publishing the image

Run `make publish`

Verify that the new image tags have been pushed/updated at https://hub.docker.com/r/usvc/db-migrator/tags.

## Configuring the CI Pipeline

### GitLab

Insert the following Variables into the CI/CD section of the repository:

| Key | Value |
| ---: | :--- |
| DOCKER_REGISTRY_URL | URL to your Docker registry |
| DOCKER_REGISTRY_USER | Username for your registry user |
| DOCKER_REGISTRY_PASSWORD | Password for your registry user |

# License

Content herein is licensed under the [MIT license](./LICENSE).
